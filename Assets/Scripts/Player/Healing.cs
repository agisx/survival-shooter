﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healing : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            if (Vector3.Distance(transform.position, other.transform.position) <= 1.7f) {
                PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
                if (playerHealth.currentHealth < playerHealth.startingHealth) {
                    other.gameObject.GetComponent<PlayerHealth>().currentHealth += 30;
                    Destroy(gameObject);
                }
            }
        }
    }
}
