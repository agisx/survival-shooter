﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSpeedBooster : MonoBehaviour
{

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            if (Vector3.Distance(transform.position, other.transform.position) <= 1.7f) {
                PlayerMovement playerMovement = other.gameObject.GetComponent<PlayerMovement>(); 
                if (playerMovement.currentSpeed == playerMovement.speed) { 
                    playerMovement.boosterTime += 3;
                    playerMovement.currentSpeed += 3;
                    Destroy(gameObject);
                }
            }
        }
    }
}
