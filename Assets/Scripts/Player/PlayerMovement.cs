﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;
    public float currentSpeed;
    private Vector3 movement;
    private Animator animator;
    private Rigidbody playerRigidbody;
    private int floorMask;
    private float camRayLength = 100f;

    private void Awake() {
        //mendapatkan nilai mask dari layer yang bernama Floor
        floorMask = LayerMask.GetMask("Floor");

        //Mendapatkan komponen Animator
        animator = GetComponent<Animator>();

        //Mendapatkan komponen Rigidbody
        playerRigidbody = GetComponent<Rigidbody>();

        currentSpeed = speed;
    }

    public float boosterTime = 0f;
    private void FixedUpdate() {

        boosterTime -= Time.deltaTime;
        if(boosterTime <= 0) {
            boosterTime = 0;
            currentSpeed = speed;
        } 

        //Mendapatkan nilai input horizontal (-1,0,1)
        float horizontal = Input.GetAxisRaw("Horizontal");

        //Mendapatkan nilai input vertical (-1,0,1)
        float vertical = Input.GetAxisRaw("Vertical");

        Move(horizontal, vertical);
        Turning();
        Animating(horizontal, vertical);

    }

    public void Move(float horizontal, float vertical) {
        //Set nilai x dan y
        movement.Set(horizontal, 0f, vertical);

        //Menormalisasi nilai vector agar total panjang dari vector adalah 1
        movement = movement.normalized * currentSpeed * Time.deltaTime;

        //Move to position
        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning() {
        //Buat Ray dari posisi mouse di layar
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Buat raycast untuk floorHit
        RaycastHit floorHit;

        //Lakukan raycast
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask)) {
            //Mendapatkan vector dari posisi player dan posisi floorHit
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            //Mendapatkan look rotation baru ke hit position
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            //Rotasi player
            playerRigidbody.MoveRotation(newRotation);
        }
    }
    public void Animating(float horizontal, float vertical) {
        bool walking = horizontal != 0f || vertical != 0f;
        animator.SetBool("IsWalking", walking);
    }

    private void OnDrawGizmos() {

        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition); 
    }
}
