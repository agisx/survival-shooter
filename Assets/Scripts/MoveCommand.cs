﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommand : Command
{
    PlayerMovement playerMovement;
    float horizontal, vertical;

    public MoveCommand(PlayerMovement _playerMovement, float _horizontal, float _vertical) {
        playerMovement = _playerMovement;
        horizontal = _horizontal;
        vertical = _vertical;
    }

    //Trigger perintah movement
    public override void Execute() {
        playerMovement.Move(horizontal, vertical);

        //Menganimasikan player
        playerMovement.Animating(horizontal, vertical);
    }

    public override void UnExecute() {
        //Invers arah dari movement player
        playerMovement.Move(-horizontal, -vertical);

        //Menganimasikan player
        playerMovement.Animating(horizontal, vertical);
    }
}
