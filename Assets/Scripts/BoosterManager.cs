﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterManager : MonoBehaviour{
    [Header("Booster")]
    public List<Transform> listBoosters;
    [SerializeField] private int maxBoosterSpawn = 3;

    private int BoosterOnTheGame = 0;

    public float spawnTime = 3f;

    void Update() {
        InvokeRepeating("BoosterSpawner", spawnTime, spawnTime);
    }

    public void BoosterSpawner() {
        BoosterOnTheGame = transform.childCount;
        if (BoosterOnTheGame >= maxBoosterSpawn) return;

        Transform boosterRandom = listBoosters[Random.Range(0, listBoosters.Count)];

        Instantiate(boosterRandom, gameObject.transform);
        boosterRandom.gameObject.GetComponent<FixPositions>().SpawnUpAgain();
    }
}
