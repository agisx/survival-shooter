﻿using System; 
using UnityEngine;

public class EnemyFactory : MonoBehaviour, IFactory {
    [SerializeField]
    public GameObject[] enemyPrefab;

    public GameObject FactoryMethod(string tag) {
        GameObject enemy = Instantiate(enemyPrefab[Int32.Parse(tag)]);
        return enemy;
    } 
    public GameObject FactoryMethod(int tag) {
        GameObject enemy = Instantiate(enemyPrefab[tag]);
        return enemy;
    } 
}
