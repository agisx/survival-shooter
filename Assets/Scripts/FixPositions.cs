﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixPositions : MonoBehaviour{
     
    private void OnTriggerStay(Collider other) {
        if (other.CompareTag("Environment")) {
            print("true envi");
            SpawnUpAgain();
        }
    }

    public void SpawnUpAgain() { 
        float randomPositionX = Random.Range(-10, 10);
        float randomPositionZ = Random.Range(-10, 10);
        transform.position = new Vector3(randomPositionX, transform.position.y, randomPositionZ);
    }
}
